package com.selfperfection.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.selfperfection.akka.Greeter.Greet;
import com.selfperfection.akka.Greeter.WhoToGreet;


import java.io.IOException;

public class AkkaQuickstart {
  public static void main(String[] args) {
    final ActorSystem actorSystem = ActorSystem.create("helloakka");
    try {
      //#create-actors
      final ActorRef printerActor =
        actorSystem.actorOf(Printer.props(), "printerActor");
      final ActorRef howdyGreeter =
        actorSystem.actorOf(Greeter.props("Howdy", printerActor), "howdyGreeter");
      final ActorRef helloGreeter = 
        actorSystem.actorOf(Greeter.props("Hello", printerActor), "helloGreeter");
      final ActorRef goodDayGreeter = 
        actorSystem.actorOf(Greeter.props("Good day", printerActor), "goodDayGreeter");
      //#create-actors

      //#main-send-messages
      howdyGreeter.tell(new WhoToGreet("Akka"), ActorRef.noSender());
      howdyGreeter.tell(new Greet(), ActorRef.noSender());

      howdyGreeter.tell(new WhoToGreet("Lightbend"), ActorRef.noSender());
      howdyGreeter.tell(new Greet(), ActorRef.noSender());

      helloGreeter.tell(new WhoToGreet("Java"), ActorRef.noSender());
      helloGreeter.tell(new Greet(), ActorRef.noSender());

      goodDayGreeter.tell(new WhoToGreet("Play"), ActorRef.noSender());
      goodDayGreeter.tell(new Greet(), ActorRef.noSender());
      //#main-send-messages

      System.out.println(">>> Press ENTER to exit <<<");
      System.in.read();
    } catch (IOException ignored) {
    } finally {
      actorSystem.terminate();
    }
  }
}
