package com.selfperfect.boot1.actuator;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Component
public class HealthInfoProvider2 extends AbstractHealthIndicator {
    private boolean flag;

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        flag = !flag;
        if (flag) {
            builder
                    .up()
                    .withDetail("flag", flag);
        } else {
            builder
                    .down()
                    .withDetail("flag", flag);
        }
    }
}
