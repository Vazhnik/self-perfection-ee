package com.selfperfect.boot1.actuator;

import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class MyEndpoint implements Endpoint<List<String>> {
    @Override
    public String getId() {
        return "mySuperEndpoint";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isSensitive() {
        return true;
    }

    @Override
    public List<String> invoke() {
        return Arrays.asList("This is message 1", "This is message 2");
    }
}
