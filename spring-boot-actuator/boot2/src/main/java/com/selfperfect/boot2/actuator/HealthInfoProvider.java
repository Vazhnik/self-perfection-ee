package com.selfperfect.boot2.actuator;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class HealthInfoProvider implements HealthIndicator {
    private boolean flag = true;

    @Override
    public Health health() {
        flag = !flag;
        if (flag) {
            return Health.up()
                         .withDetail("flag", flag)
                         .build();
        }
        return Health.down()
                     .withDetail("flag", flag)
                     .build();
    }
}
