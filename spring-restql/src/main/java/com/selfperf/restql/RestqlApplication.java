package com.selfperf.restql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestqlApplication.class, args);
	}
}
