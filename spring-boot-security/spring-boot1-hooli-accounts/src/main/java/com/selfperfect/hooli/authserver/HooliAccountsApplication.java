package com.selfperfect.hooli.authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * Auth Server.
 */
@SpringBootApplication
@EnableResourceServer
public class HooliAccountsApplication {

    public static void main(String[] args) {
        SpringApplication.run(HooliAccountsApplication.class, args);
    }
}
