## Terms

Resource owner - User, owner of google, facebook etc. accounts.

Client - application which want to have access to Resource

Authorization Server - System that could be used by user to authorize some client to access his data

Resource Server - System that actually holds user's data.

Authorization grant - thing that proves, that user click "yes" to allow some system to be authorized and have access to user data.

Redirect URI (Callback) - When user said "yes" to give access, authorization server should have this Redirect URI to know where it should return data.

Access Token - thing that will be used to get access to some system

Note: Often Authorization server and Resource Server is the same system.


## Channels
Front Channel

Back Channel

## Flows
#### Authorization code flow
Uses both front and back channels.
Response type: code

#### Implicit flow
Uses front channel only.
Response type: token
Used when no backend: Angular/React/JS application

#### Resource owner password flow


#### Client credentials flow

## Grant types
code
token
... todo

## Scopes
Scope lists different permissions that could be requested.
For example:
contacts wall.read wall.delete openid
