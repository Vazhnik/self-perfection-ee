<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%-- Oauth form --%>
<form action="http://localhost:8091/hooli-accounts/oauth/authorize" method="post">
    <input type="hidden" name="response_type" value="code"/>
    <input type="hidden" name="client_id" value="HiSenderClientId"/>
    <input type="hidden" name="client_secret" value="hi_secret"/>
    <input type="hidden" name="redirect_uri" value="http://localhost:8080/hi-sender/securedPage"/>
    <input type="hidden" name="username" value="mikita"/>
    <button type="submit">Log in with Hooli+</button>
</form>
</body>
</html>
