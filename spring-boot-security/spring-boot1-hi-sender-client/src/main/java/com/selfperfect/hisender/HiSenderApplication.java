package com.selfperfect.hisender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Resource Server.
 */
@SpringBootApplication
public class HiSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(HiSenderApplication.class, args);
    }
}
