package com.selfperfect.hisender.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class JspController {

    @GetMapping("/page")
    public String page() {
        return "buttonPage";
    }

    @GetMapping("/securedPage")
    public String secured() {
        return "securedPage";
    }
}
