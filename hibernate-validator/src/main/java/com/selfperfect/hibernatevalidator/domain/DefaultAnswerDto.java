package com.selfperfect.hibernatevalidator.domain;

public class DefaultAnswerDto {
    private String msg;

    public DefaultAnswerDto(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
