package com.selfperfect.hibernatevalidator.domain;

import com.selfperfect.hibernatevalidator.validation.constraint.IncompatibleFlags;
import com.selfperfect.hibernatevalidator.validation.group.CreateFirstGroup;
import com.selfperfect.hibernatevalidator.validation.group.CreateSecondGroup;
import com.selfperfect.hibernatevalidator.validation.group.UpdateFirstGroup;
import com.selfperfect.hibernatevalidator.validation.group.UpdateSecondGroup;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@IncompatibleFlags(groups = {CreateSecondGroup.class, UpdateSecondGroup.class})
public class UserDto {
    @NotNull(message = "User id is required for update", groups = UpdateFirstGroup.class)
    private Long id;
    @NotEmpty(message = "User name is required.",
              groups = {CreateFirstGroup.class, UpdateFirstGroup.class})
    private String name;
    private LocalDate birthday;
    private Double cash;
    private String password;
    private Boolean incompatibleFlag1;
    private Boolean incompatibleFlag2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIncompatibleFlag1() {
        return incompatibleFlag1;
    }

    public void setIncompatibleFlag1(Boolean incompatibleFlag1) {
        this.incompatibleFlag1 = incompatibleFlag1;
    }

    public Boolean getIncompatibleFlag2() {
        return incompatibleFlag2;
    }

    public void setIncompatibleFlag2(Boolean incompatibleFlag2) {
        this.incompatibleFlag2 = incompatibleFlag2;
    }
}
