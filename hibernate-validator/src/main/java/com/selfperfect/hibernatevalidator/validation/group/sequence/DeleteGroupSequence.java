package com.selfperfect.hibernatevalidator.validation.group.sequence;

import com.selfperfect.hibernatevalidator.validation.group.DeleteFirstGroup;

import javax.validation.GroupSequence;

@GroupSequence({DeleteFirstGroup.class})
public interface DeleteGroupSequence {
}
