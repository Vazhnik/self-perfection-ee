package com.selfperfect.hibernatevalidator.validation.validator;

import com.selfperfect.hibernatevalidator.domain.UserDto;
import com.selfperfect.hibernatevalidator.validation.constraint.IncompatibleFlags;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IncompatibleFlagsValidator implements ConstraintValidator<IncompatibleFlags, UserDto> {
    /**
     * Used to specify more details about root cause - will give more details in response.
     */
    private static final String FLAG1_PROPERTY_NODE = "incompatibleFlag1";
    private IncompatibleFlags constraint;

    @Override
    public void initialize(IncompatibleFlags constraintAnnotation) {
        this.constraint = constraintAnnotation;
    }

    @Override
    public boolean isValid(UserDto userDto, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        Boolean flag1 = userDto.getIncompatibleFlag1();
        Boolean flag2 = userDto.getIncompatibleFlag2();
        if (flag1 == null && flag2 == null) {
            context.buildConstraintViolationWithTemplate(constraint.allFlagsAreNullMessage())
                    .addPropertyNode(FLAG1_PROPERTY_NODE)
                    .addConstraintViolation();
            return false;
        }

        if (flag1 == flag2) {
            context.buildConstraintViolationWithTemplate(constraint.message())
                    .addPropertyNode(FLAG1_PROPERTY_NODE)
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}
