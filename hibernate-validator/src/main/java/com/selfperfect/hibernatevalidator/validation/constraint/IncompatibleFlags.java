package com.selfperfect.hibernatevalidator.validation.constraint;

import com.selfperfect.hibernatevalidator.validation.validator.IncompatibleFlagsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IncompatibleFlagsValidator.class)
public @interface IncompatibleFlags {
    String message() default "{com.selfperfect.hibernatevalidator.validation.constraint.IncompatibleFlags.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String allFlagsAreNullMessage() default
            "{com.selfperfect.hibernatevalidator.validation.constraint.IncompatibleFlags.allFlagsAreNull.message}";
}
