package com.selfperfect.hibernatevalidator.validation.group.sequence;

import com.selfperfect.hibernatevalidator.validation.group.UpdateFirstGroup;
import com.selfperfect.hibernatevalidator.validation.group.UpdateSecondGroup;

import javax.validation.GroupSequence;

@GroupSequence({
        UpdateFirstGroup.class,
        UpdateSecondGroup.class,
        })
public interface UpdateGroupSequence {
}
