package com.selfperfect.hibernatevalidator.validation.group.sequence;

import com.selfperfect.hibernatevalidator.validation.group.CreateFirstGroup;
import com.selfperfect.hibernatevalidator.validation.group.CreateSecondGroup;
import com.selfperfect.hibernatevalidator.validation.group.CreateThirdGroup;

import javax.validation.GroupSequence;

@GroupSequence({
        CreateFirstGroup.class,
        CreateSecondGroup.class,
        CreateThirdGroup.class
        })
public interface CreateGroupSequence {
}
