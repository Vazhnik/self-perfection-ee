package com.selfperfect.hibernatevalidator.controller;

import com.selfperfect.hibernatevalidator.domain.DefaultAnswerDto;
import com.selfperfect.hibernatevalidator.domain.UserDto;
import com.selfperfect.hibernatevalidator.validation.group.sequence.CreateGroupSequence;
import com.selfperfect.hibernatevalidator.validation.group.sequence.DeleteGroupSequence;
import com.selfperfect.hibernatevalidator.validation.group.sequence.UpdateGroupSequence;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/")
public class NightClubController {

    @GetMapping
    public ResponseEntity<DefaultAnswerDto> index() {
        return ResponseEntity.ok(new DefaultAnswerDto("Hi there! Post User to check the validations."));
    }

    @PostMapping
    public ResponseEntity<DefaultAnswerDto> create(
            @Validated({CreateGroupSequence.class}) @RequestBody UserDto userDto) {
        return ResponseEntity.ok(new DefaultAnswerDto("Created"));
    }

    @PutMapping
    public ResponseEntity<DefaultAnswerDto> update(
            @Validated({UpdateGroupSequence.class}) @RequestBody UserDto userDto) {
        return ResponseEntity.ok(new DefaultAnswerDto("Updated"));
    }

    @DeleteMapping
    public ResponseEntity<DefaultAnswerDto> delete(@Validated({DeleteGroupSequence.class}) @RequestParam long id) {
        return ResponseEntity.ok(new DefaultAnswerDto("Deleted"));
    }
}
