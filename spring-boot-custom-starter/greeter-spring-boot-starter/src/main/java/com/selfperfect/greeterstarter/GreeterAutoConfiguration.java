package com.selfperfect.greeterstarter;

import com.baeldung.greeter.Greeter;
import com.baeldung.greeter.GreetingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.baeldung.greeter.GreeterConfigParams.AFTERNOON_MESSAGE;
import static com.baeldung.greeter.GreeterConfigParams.EVENING_MESSAGE;
import static com.baeldung.greeter.GreeterConfigParams.MORNING_MESSAGE;
import static com.baeldung.greeter.GreeterConfigParams.NIGHT_MESSAGE;
import static com.baeldung.greeter.GreeterConfigParams.USER_NAME;

@Configuration
@ConditionalOnClass(Greeter.class)
@EnableConfigurationProperties(GreeterPropertiesAccessor.class)
public class GreeterAutoConfiguration {

    @Autowired
    private GreeterPropertiesAccessor greeterPropertiesAccessor;

    @Bean
    @ConditionalOnMissingBean
    public GreetingConfig greetingConfig() {
        String userName = retrieveSystemIfAbsent(greeterPropertiesAccessor.getUserName(), "user.name");
        String morningMsg = retrieveSystemIfAbsent(greeterPropertiesAccessor.getMorningMessage(), "morning.message");
        String afternoonMsg = retrieveSystemIfAbsent(greeterPropertiesAccessor.getAfternoonMessage(), "afternoon.message");
        String eveningMsg = retrieveSystemIfAbsent(greeterPropertiesAccessor.getEveningMessage(), "evening.message");
        String nightMsg = retrieveSystemIfAbsent(greeterPropertiesAccessor.getNightMessage(), "night.message");

        GreetingConfig greetingConfig = new GreetingConfig();
        greetingConfig.put(USER_NAME, userName);
        greetingConfig.put(MORNING_MESSAGE, morningMsg);
        greetingConfig.put(AFTERNOON_MESSAGE, afternoonMsg);
        greetingConfig.put(EVENING_MESSAGE, eveningMsg);
        greetingConfig.put(NIGHT_MESSAGE, nightMsg);
        return greetingConfig;
    }

    @Bean
    @ConditionalOnMissingBean
    public Greeter greeter(GreetingConfig greetingConfig) {
        return new Greeter(greetingConfig);
    }

    private String retrieveSystemIfAbsent(String msg, String systemPropertyKey) {
        return msg == null ? System.getProperty(systemPropertyKey) : msg;
    }
}
