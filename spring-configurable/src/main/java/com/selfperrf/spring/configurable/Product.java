package com.selfperrf.spring.configurable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.Date;

@Configurable(preConstruction = true)
public class Product {

    private final Date createDate;

    @Autowired
    private Environment environment;

    public Product() {
        this.createDate = environment.getCurrentDate();
    }

    public Date getCreateDate() {
        return createDate;
    }
}
