package com.selfperrf.spring.configurable;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(Config.class);
//        System.out.println(context.getBean(Product.class).getCreateDate());
        System.out.println(new Product().getCreateDate());
    }
}
