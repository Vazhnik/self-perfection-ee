package com.selfperrf.spring.configurable;

import java.util.Calendar;
import java.util.Date;

public class Environment {
    private Date date = Calendar.getInstance().getTime();

    public Date getCurrentDate() {
        return this.date;
    }
}
