package com.selfperrf.spring.b;

public class XComponent {

    private XConfigurable xConfigurable;

    public XComponent(XConfigurable xConfigurable) {
        this.xConfigurable = xConfigurable;
    }

    @Override
    public String toString() {
        return "XComponent{" +
                "xConfigurable=" + xConfigurable +
                '}';
    }
}
