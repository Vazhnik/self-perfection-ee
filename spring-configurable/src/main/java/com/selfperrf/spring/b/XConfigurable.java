package com.selfperrf.spring.b;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class XConfigurable {

    @Autowired
    private ComponentToWire componentToWire;

    @Override
    public String toString() {
        return "XConfigurable{" +
                "componentToWire=" + componentToWire +
                '}';
    }
}
