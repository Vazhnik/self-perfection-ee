package com.selfperrf.spring.b;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

@Configuration
@EnableSpringConfigured
public class Config {

    @Bean
    public XComponent xComponent() {
        return new XComponent(new XConfigurable());
    }
}
